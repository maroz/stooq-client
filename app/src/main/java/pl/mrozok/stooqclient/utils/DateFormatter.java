package pl.mrozok.stooqclient.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateFormatter {

    private static final SimpleDateFormat PARSER_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    private static final SimpleDateFormat ROW_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'\n'HH:mm:ss", Locale.getDefault());

    public static Date parseDate(String date) throws ParseException {
        return PARSER_DATE_FORMAT.parse(date);
    }

    public static String formatDate(Date date) {
        return ROW_DATE_FORMAT.format(date);
    }
}
