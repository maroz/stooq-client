package pl.mrozok.stooqclient.utils;

public interface Destroyable {
    void destroy();
}
