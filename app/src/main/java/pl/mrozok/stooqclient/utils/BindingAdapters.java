package pl.mrozok.stooqclient.utils;

import android.databinding.BindingAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

public final class BindingAdapters {

    @BindingAdapter("onRefreshListener")
    public static void setOnRefreshListener(SwipeRefreshLayout view, OnRefreshListener listener) {
        view.setOnRefreshListener(listener);
    }

    @BindingAdapter("refreshing")
    public static void setRefreshing(SwipeRefreshLayout view, boolean refreshing) {
        view.setRefreshing(refreshing);
    }
}
