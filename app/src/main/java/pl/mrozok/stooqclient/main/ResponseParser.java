package pl.mrozok.stooqclient.main;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import pl.mrozok.stooqclient.data.Index;
import pl.mrozok.stooqclient.utils.DateFormatter;
import pl.mrozok.stooqclient.utils.Destroyable;

class ResponseParser implements Destroyable {

    private static final String TAG = ResponseParser.class.getSimpleName();
    private static final String TIME_CONCAT_FORMAT = "%s %s";

    private static final int NAME = 0;
    private static final int DATE = 1;
    private static final int TIME = 2;
    private static final int VALUE = 6;

    private HandlerThread thread;
    private Handler handler;
    private OnParsingResponseFinishListener listener = getEmptyListener();

    @Override
    public void destroy() {
        if (null != thread) {
            handler.removeCallbacksAndMessages(null);
            thread.quit();
            handler = null;
            thread = null;
        }
    }

    ArrayList<Index> getIndices(String response) throws IOException {
        CSVReader reader = new CSVReader(new StringReader(response), ',');
        ArrayList<Index> indices = new ArrayList<>();
        String[] row = reader.readNext();
        if (null == row)
            return indices;

        while (null != (row = reader.readNext()) && 0 < row.length) {
            Index index = parseIndex(row);
            indices.add(index);
        }
        return indices;
    }

    void getIndicesAsync(String response) {
        initHandlerIfShould();
        handler.post(createGetIndicesRunnable(response));
    }

    private Runnable createGetIndicesRunnable(final String response) {
        return () -> {
            try {
                ArrayList<Index> indices = getIndices(response);
                new Handler(Looper.getMainLooper()).post(() -> listener.onParsingResponseFinish(indices));
            } catch (IOException ignore) {
            }
        };
    }

    private void initHandlerIfShould() {
        if (null == thread) {
            thread = new HandlerThread(TAG);
            thread.start();
            handler = new Handler(thread.getLooper());
        }
    }

    private Index parseIndex(String[] row) {
        Index index = new Index();
        index.setName(row[NAME]);
        fillRefreshedAtFromRow(index, row);
        fillValueFromRow(index, row);
        return index;
    }

    private void fillRefreshedAtFromRow(Index index, String[] row) {
        try {
            String date = String.format(TIME_CONCAT_FORMAT, row[DATE], row[TIME]);
            index.setRefreshedAt(DateFormatter.parseDate(date));
        } catch (ParseException ignore) {
        }
    }

    private void fillValueFromRow(Index index, String[] row) {
        try {
            double value = Double.parseDouble(row[VALUE]);
            index.setValue(value);
        } catch (NumberFormatException ignore) {
        }
    }

    public void setListener(@Nullable OnParsingResponseFinishListener listener) {
        if (null == listener)
            this.listener = getEmptyListener();
        else
            this.listener = listener;
    }

    private OnParsingResponseFinishListener getEmptyListener() {
        return indices -> {
        };
    }

    interface OnParsingResponseFinishListener {
        void onParsingResponseFinish(ArrayList<Index> indices);
    }
}
