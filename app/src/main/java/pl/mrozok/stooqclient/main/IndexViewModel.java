package pl.mrozok.stooqclient.main;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.Locale;

import pl.mrozok.stooqclient.BR;
import pl.mrozok.stooqclient.data.Index;
import pl.mrozok.stooqclient.utils.DateFormatter;
import pl.mrozok.stooqclient.utils.Destroyable;

public class IndexViewModel extends BaseObservable implements Destroyable {

    private static final String VALUE_FORMAT = "%.2f";

    private Index index;

    @Override
    public void destroy() {
        index = null;
    }

    public void setIndex(Index index) {
        this.index = index;
        notifyPropertyChanged(BR.index);
        notifyPropertyChanged(BR.formattedTime);
        notifyPropertyChanged(BR.formattedValue);
    }

    @Bindable
    public Index getIndex() {
        return index;
    }

    @Bindable
    public String getFormattedValue() {
        try {
            return String.format(Locale.getDefault(), VALUE_FORMAT, index.getValue());
        } catch (NullPointerException | NumberFormatException ignore) {
            return "";
        }
    }

    @Bindable
    public String getFormattedTime() {
        try {
            return DateFormatter.formatDate(index.getRefreshedAt());
        } catch (NullPointerException ignore) {
            return "";
        }
    }
}
