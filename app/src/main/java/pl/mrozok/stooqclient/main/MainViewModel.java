package pl.mrozok.stooqclient.main;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import pl.mrozok.stooqclient.BR;
import pl.mrozok.stooqclient.data.AppPreferences;
import pl.mrozok.stooqclient.data.Index;
import pl.mrozok.stooqclient.utils.Destroyable;

public class MainViewModel extends BaseObservable implements
        Destroyable, Response.Listener<String>, Response.ErrorListener,
        ResponseParser.OnParsingResponseFinishListener, PickRefreshRateDialog.OnPickRefreshRateListener,
        Runnable {

    private static final String TAG = MainViewModel.class.getSimpleName();
    private String URL = "http://stooq.pl/q/l/?s=wig+wig20+mwig40+swig80&f=sd2t2ohlc&h&e=csv";

    private Context context;
    private AppPreferences preferences;
    private ResponseParser parser;
    private IndexListAdapter listAdapter;
    private Handler handler;
    private HandlerThread thread;
    private PickRefreshRateDialog pickRefreshRateDialog;

    private boolean listVisible = true;
    private int refreshRateInMillis;

    public MainViewModel(Context context, AppPreferences preferences) {
        this.context = context;
        this.preferences = preferences;
        this.parser = new ResponseParser();
        this.parser.setListener(this);
        this.pickRefreshRateDialog = new PickRefreshRateDialog(context, this);
        initRefreshRate();
        initHandler();
        refreshIndices();
    }

    private void initRefreshRate() {
        int index = preferences.getRefreshRate();
        setRefreshRateInMillis(PickRefreshRateDialog.VALUES[index]);
    }

    private void initHandler() {
        thread = new HandlerThread(TAG);
        thread.start();
        handler = new Handler(thread.getLooper());
    }
g
    @Override
    public void destroy() {
        destroyHandler();
        parser.destroy();
        parser = null;
        listAdapter.destroy();
        listAdapter = null;
        preferences = null;
        context = null;
    }

    void resume() {
        scheduleRefresh();
    }

    private void scheduleRefresh() {
        handler.postDelayed(this, refreshRateInMillis);
    }

    @Override
    public void run() {
        refreshIndices();
    }

    void pause() {
        handler.removeCallbacksAndMessages(null);
    }

    private void destroyHandler() {
        thread.quit();
        handler = null;
        thread = null;
    }

    public void onPickRefreshRateClick() {
        pickRefreshRateDialog.show(preferences.getRefreshRate());
    }

    @Override
    public void onPickRefreshRate(int index) {
        preferences.setRefreshRate(index);
        setRefreshRateInMillis(PickRefreshRateDialog.VALUES[index]);
        handler.removeCallbacksAndMessages(null);
        scheduleRefresh();
    }

    private void refreshIndices() {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest =
                new StringRequest(Request.Method.GET, URL, this, this);
        queue.add(stringRequest);
    }

    @Override
    public void onResponse(String response) {
        parser.getIndicesAsync(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        notifyRefreshing();
        setListVisible(false);
        Log.e(TAG, "getting response error", error);
    }

    @Override
    public void onParsingResponseFinish(ArrayList<Index> indices) {
        listAdapter.addValues(indices);
        setListVisible(true);
        notifyRefreshing();
    }

    void notifyRefreshing() {
        notifyPropertyChanged(BR.refreshing);
    }

    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return () -> {
            refreshIndices();
        };
    }

    public IndexListAdapter createAdapter() {
        if (null == listAdapter)
            listAdapter = new IndexListAdapter(context);
        return listAdapter;
    }

    @Bindable
    public boolean isRefreshing() {
        return false;
    }

    private void setListVisible(boolean listVisible) {
        this.listVisible = listVisible;
        notifyPropertyChanged(BR.listVisible);
    }

    @Bindable
    public boolean isListVisible() {
        return listVisible;
    }

    void setRefreshRateInMillis(int refreshRateInSeconds) {
        this.refreshRateInMillis = refreshRateInSeconds * 1000;
    }
}
