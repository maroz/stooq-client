package pl.mrozok.stooqclient.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import pl.mrozok.stooqclient.R;
import pl.mrozok.stooqclient.StooqClientApp;
import pl.mrozok.stooqclient.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBinding();
    }

    private void initBinding() {
        viewModel = new MainViewModel(this, getApp().getPreferences());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);
        setSupportActionBar(binding.toolbar);
        initRecyclerView();
    }

    private StooqClientApp getApp() {
        return (StooqClientApp) getApplication();
    }

    private void initRecyclerView() {
        IndexListAdapter adapter = viewModel.createAdapter();

        RecyclerView list = binding.list;
        list.setHasFixedSize(true);
        list.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        viewModel.destroy();
        viewModel = null;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.resume();
    }

    @Override
    protected void onPause() {
        viewModel.pause();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.pickRefreshRate == item.getItemId()) {
            viewModel.onPickRefreshRateClick();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
