package pl.mrozok.stooqclient.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.mrozok.stooqclient.data.Index;
import pl.mrozok.stooqclient.databinding.RowIndexBinding;
import pl.mrozok.stooqclient.utils.Destroyable;

public class IndexListAdapter extends RecyclerView.Adapter<IndexListAdapter.IndexViewHolder>
        implements Destroyable {

    private ArrayList<Index> indices = new ArrayList<>();

    private LayoutInflater inflater;

    public IndexListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroy() {
        indices = null;
        inflater = null;
    }

    @Override
    public IndexViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return IndexViewHolder.create(inflater, parent);
    }

    @Override
    public void onBindViewHolder(IndexViewHolder holder, int position) {
        holder.bindTo(indices.get(position));
    }

    @Override
    public int getItemCount() {
        return indices.size();
    }

    public void addValues(ArrayList<Index> dataSet) {
        for (int i = 0; i < dataSet.size(); i++) {
            Index index = dataSet.get(i);
            updateValue(index);
        }
    }

    private void updateValue(Index index) {
        String name = index.getName();
        Index old = findIndexByName(name);
        if (null == old)
            insertItem(index);
        else
            updateItem(old, index);
    }

    private Index findIndexByName(String name) {
        for (int i = 0; i < indices.size(); i++) {
            Index index = indices.get(i);
            if (name.equals(index.getName()))
                return index;
        }
        return null;
    }

    private void insertItem(Index index) {
        indices.add(index);
        notifyItemInserted(indices.size());
    }

    private void updateItem(Index old, Index current) {
        if (!old.getRefreshedAt().equals(current.getRefreshedAt())) {
            int index = indices.indexOf(old);
            old.setRefreshedAt(current.getRefreshedAt());
            if (old.getValue() != current.getValue())
                old.setValue(current.getValue());
            notifyItemChanged(index);
        }
    }

    static class IndexViewHolder extends ViewHolder {

        private RowIndexBinding binding;

        static IndexViewHolder create(LayoutInflater inflater, ViewGroup parent) {
            RowIndexBinding binding = RowIndexBinding.inflate(inflater, parent, false);
            binding.setViewModel(new IndexViewModel());
            return new IndexViewHolder(binding);
        }

        IndexViewHolder(RowIndexBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindTo(Index index) {
            binding.getViewModel().setIndex(index);
        }
    }
}
