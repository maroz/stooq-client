package pl.mrozok.stooqclient.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSeekBar;
import android.widget.SeekBar;
import android.widget.TextView;

import pl.mrozok.stooqclient.R;
import pl.mrozok.stooqclient.utils.Destroyable;

class PickRefreshRateDialog
        implements OnClickListener, Destroyable, SeekBar.OnSeekBarChangeListener {

    public static final int[] VALUES = {20, 30, 60, 120, 180};

    private Context context;
    private OnPickRefreshRateListener listener;
    private AlertDialog dialog;
    private AppCompatSeekBar value;
    private TextView label;
    private String labelFormat;

    public PickRefreshRateDialog(Context context, OnPickRefreshRateListener listener) {
        this.context = context;
        this.listener = listener;
        this.labelFormat = context.getString(R.string.refresh_rate_label_format);
    }

    AlertDialog show(int index) {
        if (null == dialog) {
            dialog = createDialog();
            dialog.show();
            value = (AppCompatSeekBar) dialog.findViewById(R.id.value);
            value.setOnSeekBarChangeListener(this);
            value.setProgress(index);
            label = (TextView) dialog.findViewById(R.id.label);
            updateLabel();
        } else
            dialog.show();
        return dialog;
    }

    private AlertDialog createDialog() {
        return new AlertDialog.Builder(context)
                .setTitle(R.string.refresh_rate)
                .setView(R.layout.dialog_pick_refresh_rate)
                .setNeutralButton(R.string.cancel, null)
                .setPositiveButton(R.string.select, this)
                .create();
    }

    @Override
    public void destroy() {
        context = null;
        dialog = null;
        listener = null;
        value = null;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        listener.onPickRefreshRate(value.getProgress());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser)
            updateLabel();
    }

    private void updateLabel() {
        int refreshRate = VALUES[value.getProgress()];
        String labelText = String.format(labelFormat, refreshRate);
        label.setText(labelText);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    interface OnPickRefreshRateListener {
        void onPickRefreshRate(int index);
    }
}
