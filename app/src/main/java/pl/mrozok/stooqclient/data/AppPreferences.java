package pl.mrozok.stooqclient.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class AppPreferences {

    private static final String PREF_REFRESH_RATE = "refresh_rate";

    private SharedPreferences preferences;

    public AppPreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setRefreshRate(int refreshRate) {
        Editor editor = preferences.edit();
        editor.putInt(PREF_REFRESH_RATE, refreshRate);
        editor.apply();
    }

    public int getRefreshRate() {
        return preferences.getInt(PREF_REFRESH_RATE, 1);
    }
}
