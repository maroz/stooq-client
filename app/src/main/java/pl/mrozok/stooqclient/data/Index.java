package pl.mrozok.stooqclient.data;

import java.util.Date;

public class Index {

    private String name;
    private Date refreshedAt;
    private double value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRefreshedAt() {
        return refreshedAt;
    }

    public void setRefreshedAt(Date refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
