package pl.mrozok.stooqclient;

import android.app.Application;

import pl.mrozok.stooqclient.data.AppPreferences;

public class StooqClientApp extends Application {

    private AppPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = new AppPreferences(this);
    }

    public AppPreferences getPreferences() {
        return preferences;
    }
}
